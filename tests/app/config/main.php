<?php
$mysql = [
    'class'=>\yii\db\Connection::class,
    'dsn' => getenv('MYSQL_DSN'),
    'username' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASSWORD'),
    'tablePrefix' => getenv('MYSQL_PREFIX')
];
$pgsql = [
    'class'=>\yii\db\Connection::class,
    'dsn' => getenv('PG_DSN'),
    'username' => getenv('PG_USER'),
    'password' => getenv('PG_PASSWORD'),
    'tablePrefix' => getenv('PG_PREFIX')
];
/*
$redis = [
    'class'=>\yii\redis\Connection::class,
    'host'=>getenv('REDIS_HOST'),
    'port'=>getenv('REDIS_PORT'),
    'password'=>getenv('REDIS_PASSWORD')
];*/
return [
    'id' => 'test-app',
    'basePath' => dirname(__DIR__).'/../',
    'vendorPath' => dirname(__DIR__) . '/../vendor',
    'sourceLanguage' => 'en-US',
    'timeZone'            => 'Europe/Moscow',
    'language'       => 'ru',
    'charset'        => 'utf-8',
    'bootstrap'=>['log'],
    'aliases'=>[],
    'container'=>[
        'definitions'=>[],
        'singletons'=>[]
    ],
    'components' => [
        'db'=>$mysql,
        'mysql'=>$mysql,
        'pgsql'=>$pgsql,
        'dummy'=>[
            'class'=>\solly\dummyunitext\Dummy::class
        ]
    ],
    'modules'=>[],
    'params'=>[]
];