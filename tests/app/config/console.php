<?php
return [
    'controllerMap'=>[
        'pg-migrate'=>[
            'class' => \yii\console\controllers\MigrateController::class,
            'db' => 'pgsql',
            'migrationPath' => getenv('MIGRATION_PATH').'/pgsql',
        ],
        'mysql-migrate'=>[
            'class' => \yii\console\controllers\MigrateController::class,
            'db' => 'mysql',
            'migrationPath' => getenv('MIGRATION_PATH').'/mysql',
        ],
    ]
];