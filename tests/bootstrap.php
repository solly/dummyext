<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', __DIR__);

require_once(YII_APP_BASE_PATH . '/vendor/autoload.php');
$dotenv = new \Dotenv\Dotenv(YII_APP_BASE_PATH);
$dotenv->load();
require_once(YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php');
$config = require(__DIR__ . '/app/config/main.php');
$app = new \yii\console\Application($config);
\Yii::setAlias('@tests', __DIR__);
