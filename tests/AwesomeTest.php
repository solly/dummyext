<?php
/**
 * Created by solly [24.05.17 14:43]
 */

namespace tests;

use solly\dummyunitext\Dummy;

class AwesomeTest extends TestCase
{
    public function testDummy()
    {
        $dummy = new Dummy();
        $this->assertEquals(5, $dummy->sum(2, 3));
    }
}
