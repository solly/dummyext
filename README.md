qwertyuio
=========
qwertyuiop

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist solly/yii2-dummyunitext "*"
```

or add

```
"solly/yii2-dummyunitext": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \solly\dummyunitext\AutoloadExample::widget(); ?>```