<?php

namespace solly\dummyunitext;

use yii\base\Component;

/**
 * This is just an example.
 */
class Dummy extends Component
{
    public function sum($a, $b)
    {
        return $a + $b;
    }
}
